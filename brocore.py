# -*- coding: utf-8 -*-

import asyncio

from core.error import CoreError
from core.service import Core

if __name__ == "__main__":

    loop = asyncio.get_event_loop()
    core = Core()

    try:
        loop.run_until_complete(core.start(loop))
        loop.run_forever()
    except CoreError:
        loop.run_until_complete(core.exit(loop))
    finally:
        loop.close()
