# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903
import asyncio

from utils.config import RPC_LISTEN, RPC_PORT
from utils.base_service import BaseService
from rpc.protocol import RpcProtocol


class Rpc(BaseService):
    def __init__(self):
        super().__init__()
        self._server = None
        self._hostname = None

    async def start(self, *args, **kwargs):
        if RPC_PORT:
            loop = asyncio.get_event_loop()

            self._hostname = kwargs['hostname']
            self._server = await loop.create_server(
                lambda: RpcProtocol(**kwargs),
                RPC_LISTEN, RPC_PORT,
                reuse_port=True)

            self._logger.info("RPC started on %s, listening %s", self._hostname, self.socket.getsockname())

    async def stop(self):
        try:
            self._server.close()
            await self._server.wait_closed()
        except AttributeError:
            pass

    @property
    def socket(self):
        return self._server.sockets[0] if self._server.sockets else None
