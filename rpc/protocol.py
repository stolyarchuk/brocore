# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import asyncio

from utils.logger import Logger


class RpcProtocol(asyncio.Protocol, Logger):
    def __init__(self, **kwargs):
        super().__init__()
        self._user_manager = kwargs['user_manager']
        self._task_pool = kwargs['task_pool']
        self._transport = None

    def connection_made(self, transport):
        peername = transport.get_extra_info('peername')
        self._logger.debug('Connection from %s', peername)
        self._transport = transport

    def data_received(self, data):
        self._parse(data)
        self._logger.debug("Close the client socket")
        self._transport.close()

    def _parse(self, databytes):
        if not databytes:
            return

        data = databytes.decode()

        if data[0] != "#":
            self._logger.warning("bad magic byte: %s", data)
            return

        words = data[1:-2].split(';')

        if len(words) != 3:
            self._logger.warning("bad string: %s", data)
            return

        (vpnlogin, vpnip, command) = words

        if command == '1':
            self._task_pool.put_nowait(self._add_user(vpnlogin, vpnip), 0)
        elif command == '0':
            self._task_pool.put_nowait(self._del_user(vpnlogin, vpnip), 0)

    async def _add_user(self, vpnlogin, vpnip):
        user = await self._user_manager.create_user(vpnlogin, vpnip)

        if user and user.is_valid:
            self._task_pool.put_nowait(self._user_manager.add_user(user), 0)

    async def _del_user(self, vpnlogin, vpnip):
        user = await self._user_manager.get_user(vpnlogin, vpnip)

        if user:
            self._task_pool.put_nowait(self._user_manager.del_user(user), 0)
