# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import asyncio
import random

from core.error import ProxyCheckerLimitError
from shaper.service import Shaper
from user_manager.user import User
from utils.base_service import BaseService
from utils.executor import Executor
from utils.config import (OPENVPN_UPDATE_INTERVAL, PROXY_CHECKER_SCRIPT_BAD,
                          PROXY_CHECKER_SCRIPT_OK,
                          PROXY_CHECKER_UPDATE_INTERVAL)
from utils.timer import Timer


class UserManager(BaseService):
    # pylint: disable=too-many-instance-attributes
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._shaper = Shaper()
        self._timers = dict()
        self._users = set()
        self._db = None
        self._task_pool = None
        self._openvpn = None
        self._update_timer = Timer()
        self._executor = Executor()

    async def start(self, *args, **kwargs):
        await self._shaper.start(**kwargs)
        self._db = kwargs['db']
        self._task_pool = kwargs['task_pool']
        self._openvpn = kwargs['openvpn']

        await self._db.reset_user_status_by_node(kwargs['hostname'])
        await self._update_users_status()

        if OPENVPN_UPDATE_INTERVAL:
            self._update_timer.start(self._update_users_status, 0, OPENVPN_UPDATE_INTERVAL, name='OpenVPN')

    async def stop(self):
        if self._update_timer:
            self._update_timer.stop()

        for _, timer in self._timers.items():
            timer.stop()

        await self._shaper.stop()

    @property
    def users(self):
        return self._users

    async def create_user(self, *args):
        vpnlogin, vpnip = args

        if vpnlogin != "UNDEF" and vpnip:
            return await self._db.make_user_from_db(vpnlogin=vpnlogin, vpnip=vpnip)

    async def get_user(self, *args):
        vpnlogin, vpnip = args
        managed_users = list()

        if vpnlogin != "UNDEF" and vpnip:
            managed_users = [x for x in self._users if x.data.vpnip == vpnip and x.data.vpnlogin == vpnlogin]

        elif vpnlogin == "UNDEF" and vpnip:
            managed_users = [x for x in self._users if x.data.vpnip == vpnip]

        elif vpnlogin != "UNDEF" and not vpnip:
            managed_users = [x for x in self._users if x.data.vpnlogin == vpnlogin]

        if managed_users:
            return managed_users[0]

    async def get_or_create_user(self, *args):
        user = await self.get_user(*args)

        if not user:
            user = await self.create_user(*args)

        return user

    async def add_user(self, user):
        if user not in self._users:
            user.data.status = 1

            await self._start_user_tasks(user)
            await self._shaper.add_shape(user)
            await self._db.set_user_status(user)

            self._logger.info("Online: %s (vpnip: %s; total: %d)", user.data.vpnlogin, user.data.vpnip, len(self._users))
        else:
            self._task_pool.put_nowait(self._replace_user(user), 3)

    async def _start_user_tasks(self, user: User):
        self._users.add(user)

        if PROXY_CHECKER_UPDATE_INTERVAL:
            delay = random.randrange(int(PROXY_CHECKER_UPDATE_INTERVAL))
            timer = Timer()
            timer.start(self._add_user_check_proxy_task, delay, PROXY_CHECKER_UPDATE_INTERVAL, user=user)

            self._timers.update({user: timer})

    async def _add_user_check_proxy_task(self, **kwargs):
        user = kwargs.get('user', None)
        if user and not user.is_checking:
            user.is_checking = True
            self._task_pool.put_nowait(self.check_user_proxy(user), 10)

    async def check_user_proxy(self, user: User):
        try:
            await user.check_proxy()

            if user.proxy.response.status == 200:
                await user.crawler.parse(user)

                if user.proxy.last_status == 'BAD':
                    user.proxy.last_status = 'OK'
                    await self._executor.run_async("{} {} {}".format(PROXY_CHECKER_SCRIPT_OK, user.data.vpnip, user.data.vpnlogin))

            if user.proxy.is_valid:
                user.checker.reset()
                await self._update_user_proxy(user)
            else:
                user.checker.switch()
                await self._add_user_check_proxy_task(user=user)
        except ProxyCheckerLimitError:
            if user.proxy.last_status == 'OK':
                user.proxy.last_status = 'BAD'
                await self._executor.run_async("{} {} {}".format(PROXY_CHECKER_SCRIPT_BAD, user.data.vpnip, user.data.vpnlogin))
            await self._openvpn.kill_user(user.data.vpnlogin)
            await self._update_user_proxy(user)
        except asyncio.CancelledError:
            pass
        finally:
            user.is_checking = False

    async def _update_user_proxy(self, user: User):
        try:
            await self._db.update_user_proxy(user)
            self._logger.info("User: %s (%s), proxy: %s", user.data.vpnlogin, user.data.vpnip, user.proxy)
        except RuntimeError:
            pass

    async def _stop_user_tasks(self, user: User):
        timer = self._timers.pop(user)

        if timer:
            timer.stop()

        self._users.remove(user)

    async def del_user(self, user):
        if user in self._users:
            user.data.status = 0

            await self._executor.run_async("{} {} {}".format(PROXY_CHECKER_SCRIPT_BAD, user.data.vpnip, user.data.vpnlogin))
            await self._stop_user_tasks(user)
            await self._shaper.del_shape(user)
            await self._db.set_user_status(user)

            self._logger.info("Offline: %s (total: %d)", user.data.vpnlogin, len(self._users))
        else:
            self._logger.error("No such user: %s", user)

    async def _replace_user(self, user):
        managed_users = [x for x in self._users if x.vpnlogin == user.data.vpnlogin]

        if managed_users:
            await self.del_user(managed_users[0])
            await self.add_user(user)

        self._logger.warning("Replacing reconnected user: %s", user)

    async def _update_users_status(self, *args, **kwargs):  # pylint: disable=unused-argument
        self._task_pool.put_nowait(self._update_users(), 2)

    async def _update_users(self):
        try:
            new_users, del_users = await self._fetch_openvpn_users()

            for user in del_users:
                self._task_pool.put_nowait(self.del_user(user), 2)

            for user in new_users:
                self._task_pool.put_nowait(self.add_user(user), 2)
        except RuntimeError:
            pass

    async def _fetch_openvpn_users(self):
        openvpn_users = set()
        openvpn_tuples, error = await self._openvpn.get_tuples()

        for user_tuple in openvpn_tuples:
            user = await self.get_or_create_user(*user_tuple)

            if user and user.is_valid:
                openvpn_users.add(user)
            else:
                self._logger.warning('No data for vpnlogin: %s (vpnip: %s)', *user_tuple)

        self._logger.debug("Shaper users: %d, openvpn users: %d", len(self._users), len(openvpn_users))

        new_users = openvpn_users.difference(self._users)

        if error:
            del_users = set()
        else:
            del_users = self._users.difference(openvpn_users)

        return new_users, del_users
