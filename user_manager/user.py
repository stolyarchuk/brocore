# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

from collections import OrderedDict

from proxy_checker.checker import Checker
from proxy_checker.crawler import Crawler
from proxy_checker.proxy import Proxy

from utils.logger import Logger


class UserData:
    def __init__(self, **kwargs):
        self.vpnlogin = kwargs['vpnlogin']
        self.vpnip = kwargs['vpnip']
        self.id = None
        self.limit_in = None
        self.limit_out = None
        self.class_id = None
        self.status = 1


class User(Logger):

    def __init__(self, **kwargs):
        super().__init__()
        self.data = UserData(**kwargs)
        self.proxy = Proxy(self.data.vpnip)
        self.crawler = Crawler()
        self.checker = Checker()
        self.is_checking = False

    def from_dict(self, **kwargs):
        self.data.id = kwargs['user']
        self.data.limit_in = kwargs['user_speed_in']
        self.data.limit_out = kwargs['user_speed_out']

    @property
    def is_valid(self):
        return self.data.id and self.data.vpnip and self.data.limit_in and self.data.limit_out

    @property
    def json(self):
        json_dict = OrderedDict([
            ('id', self.data.id),
            ('vpnlogin', self.data.vpnlogin),
            ('vpnip', self.data.vpnip),
            ('class_id', self.data.class_id),
            ('limit_in', self.data.limit_in),
            ('limit_out', self.data.limit_out),
            ('proxy', self.proxy.json)
        ])

        return json_dict

    async def check_proxy(self):
        self.proxy.response = await self.crawler.fetch(self.proxy, self.checker)

    def __eq__(self, other):
        return self.data.vpnlogin == other.data.vpnlogin

    def __hash__(self):
        return hash("%s" % (self.data.vpnlogin))

    def __repr__(self):
        return "<User id: %05d, class_id: %s, vpnlogin: %s, vpnip: %s>" % (self.data.id, self.data.class_id, self.data.vpnlogin, self.data.vpnip)
