/*
 * brocore agent
 *
 * build: g++ -O2 -DNDEBUG --std=c++11 -lstdc++ -o broagent broagent.cc
 * usage: broagent <core_ip> <core_port> <common_name> <pool_remote_ip> <status>
 *
 */

#include <ctime>
#include <iomanip>
#include <iostream>
#include <memory>
#include <regex>
#include <sstream>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/ip.h>
#include <sys/socket.h>
#include <unistd.h>

static std::string ts();

class TcpSocket {
 public:
  TcpSocket() {}
  ~TcpSocket() {
    close(socket_fd_);
  }

  void Open() {
    socket_fd_ = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_fd_ < 0)
      throw std::runtime_error("open socket error");

    int opts = -1;

    int err = setsockopt(socket_fd_, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                         &opts, sizeof(opts));

    if (err != 0)
      throw std::runtime_error("set socket options error");
  }

  void SetNonBlocking() {
    int opts = fcntl(socket_fd_, F_GETFL);

    if (opts < 0)
      throw std::runtime_error("socket get file status error");

    opts = (opts | O_NONBLOCK);

    if (fcntl(socket_fd_, F_SETFL, opts) < 0)
      throw std::runtime_error("socket set file status error");
  }

  void Send(const std::string& data) {
    send(socket_fd_, data.data(), data.size(), 0);
  }

  void Connect(const std::string& ip, uint16_t port) {
    sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);

    if (inet_pton(AF_INET, ip.c_str(), &serv_addr.sin_addr) <= 0)
      throw std::runtime_error("bad ip address or port number: " + ip + ":" +
                               std::to_string(port));

    if (connect(socket_fd_, (sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
      throw std::runtime_error("connection failed: " + ip + ":" +
                               std::to_string(port));
  }

 private:
  int socket_fd_;
};

class TcpClient {
 public:
  TcpClient() {
    socket_.Open();
  }

  void Connect(const std::string& ip, uint16_t port) {
    socket_.Connect(ip, port);
    socket_.SetNonBlocking();
  }

  void Send(const std::string& data) {
    socket_.Send(data);
  }

 private:
  TcpSocket socket_;
};

static std::string ts() {
  std::time_t t = std::time(nullptr);
  std::tm tm = *std::localtime(&t);

  char buffer[80];

  strftime(buffer, sizeof(buffer), "%a %b %d %H:%M:%S %Y brocore agent: ", &tm);
  return std::string(buffer);
}

static void validate_ip(const std::string& ip) {
  sockaddr_in serv_addr;

  if (inet_pton(AF_INET, ip.c_str(), &serv_addr.sin_addr) <= 0)
    throw std::runtime_error("bad ip address: " + ip);
}

static void validate_args(int argc, char** argv) {
  if (6 != argc && 5 != argc) {
    bool first = true;
    std::ostringstream os;

    for (int i = 1; i < argc; ++i) {
      if (!first)
        os << "; ";

      os << "argv[" << i << "]: " << argv[i];
      first = false;
    }

    throw std::runtime_error("args error: " + os.str());
  }
}

int main(int argc, char* argv[]) {
  try {
    validate_args(argc, argv);

    std::string common_name;
    std::string remote_ip;
    uint16_t user_status;

    const std::string core_ip = std::string(argv[1]);
    const uint16_t core_port = std::atoi(argv[2]);

    if (argc == 6) {
      common_name = std::string(argv[3]);
      remote_ip = std::string(argv[4]);
      user_status = std::atoi(argv[5]);
    } else if (argc == 5) {
      common_name = "UNDEF";
      remote_ip = std::string(argv[3]);
      user_status = std::atoi(argv[4]);
    }

    validate_ip(remote_ip);

    const std::string output_data = "#" + common_name + ";" + remote_ip + ";" +
                                    std::to_string(user_status) + "\r\n";

    TcpClient agent;
    agent.Connect(core_ip, core_port);
    agent.Send(output_data);

  } catch (std::exception& ex) {
    std::cerr << ts() << ex.what() << std::endl;
  }

  return EXIT_SUCCESS;
}
