# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

from asyncio import create_subprocess_shell, wait_for
from asyncio.subprocess import PIPE as AIOPIPE

from subprocess import PIPE, CalledProcessError, Popen

from utils.logger import Logger


class Executor(Logger):

    def run(self, command, errors=True):
        try:
            process = Popen(command, shell=True, stdout=PIPE, stderr=PIPE, close_fds=True)
            cout, cerr = process.communicate()

            self._logger.debug(command)

            if cout:
                self._logger.debug(cout.decode().strip())
            if cerr and errors:
                self._logger.error(cerr.decode().strip())

            return cout.decode().strip(), cerr.decode().strip()
        except CalledProcessError as err:
            self._logger.error("%s (%s)", err, command)

    async def run_async(self, command, errors=True):
        try:
            process = await create_subprocess_shell(command, stdout=AIOPIPE, stderr=AIOPIPE)
            cout, cerr = await wait_for(process.communicate(), timeout=5)

            self._logger.debug(command)

            if cout:
                self._logger.debug(cout.decode().strip())
            if cerr and errors:
                self._logger.error(cerr.decode().strip())

            return cout.decode().strip(), cerr.decode().strip()
        except CalledProcessError as err:
            self._logger.error("%s (%s)", err, command)
