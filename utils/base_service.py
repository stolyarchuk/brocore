# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

from utils.logger import Logger


class BaseService(Logger):
    async def start(self, *args, **kwargs):
        pass

    async def stop(self):
        pass
