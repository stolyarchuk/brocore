# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

from functools import wraps
from utils.config import MODE
from utils.logger import logger_factory

mode_logger = logger_factory("OpMode")


def mode_required(mode):
    def _mode_required(fn):
        active = (not MODE or MODE >= mode)
        @wraps(fn)
        async def wrapper(*args, **kwargs):
            if active:
                return await fn(*args, **kwargs)
            mode_logger.debug("skipped function: %s", fn.__name__)
        return wrapper
    return _mode_required
