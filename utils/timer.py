# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import asyncio

from utils.logger import Logger


class Timer(Logger):
    def __init__(self, name='default'):
        super().__init__()
        self._name = name
        self._task = None

    def start(self, callback, delay, interval, *args, **kwargs):
        if interval:
            self._task = asyncio.ensure_future(self._tick(callback, delay, interval, *args, **kwargs))
        else:
            self._task = asyncio.ensure_future(self._shot(callback, delay, *args, **kwargs))

    def stop(self):
        if self._task and not self._task.cancelled():
            self._task.cancel()

    async def _tick(self, callback, delay, interval, *args, **kwargs):
        if delay:
            await asyncio.sleep(delay)
        while True:
            try:
                await asyncio.sleep(interval)
                await self._fire(callback, *args, **kwargs)
            except asyncio.CancelledError:
                break

    async def _shot(self, callback, delay, *args, **kwargs):
        if delay:
            await asyncio.sleep(delay)
        try:
            await self._fire(callback, *args, **kwargs)
        except asyncio.CancelledError:
            pass

    async def _fire(self, callback, *args, **kwargs):
        if asyncio.iscoroutinefunction(callback):
            await callback(*args, **kwargs)
        else:
            callback(*args, **kwargs)
