# -*- coding: utf-8 -*-

import configparser
import os


BASE_DIR = os.path.dirname(os.path.realpath(__file__))
PARENT_DIR = os.path.abspath(os.path.join(BASE_DIR, os.pardir))
CONF_FILE = os.path.join(PARENT_DIR, 'brocore.conf')

CoreConfig = configparser.ConfigParser()
CoreConfig.read(CONF_FILE)


general = CoreConfig['general']
queue = CoreConfig['queue']
rpc = CoreConfig['rpc']
api = CoreConfig['api']
ssh_tunnel = CoreConfig['ssh_tunnel']
mysql = CoreConfig['mysql']
openvpn = CoreConfig['openvpn']
proxy_checker = CoreConfig['proxy_checker']


MODE = general.getint('mode', 1)
LOG_LEVEL = general.get('log_level', 'debug')
MIN_RATE = general.getint('min_rate', 256)


QUEUE_MAX_SIZE = queue.getint('max_size', 200)
QUEUE_WORKERS = queue.getint('workers', 8)

RPC_LISTEN = rpc.get('listen', '127.0.0.1')
RPC_PORT = rpc.getint('port', 45063)

API_LISTEN = api.get('listen', '127.0.0.1')
API_PORT = api.getint('port', 45065)

SSH_REMOTE_USER = ssh_tunnel.get('remote_user', 'root')
SSH_REMOTE_HOST = ssh_tunnel.get('remote_host', 'remote.ssh.com')
SSH_REMOTE_PORT = ssh_tunnel.getint('remote_port', 22)
SSH_PRIVATE_KEY = ssh_tunnel.get('private_key', '/path/to/id_rsa')
SSH_LOCAL_HOST = ssh_tunnel.get('local_host', '127.0.0.1')
SSH_LOCAL_PORT = ssh_tunnel.getint('local_port', 3306)

MYSQL_HOST = mysql.get('host', '127.0.0.1')
MYSQL_PORT = mysql.getint('port', 3306)
MYSQL_USER = mysql.get('user', 'root')
MYSQL_PASS = mysql.get('pass', 'password')
MYSQL_DB = mysql.get('db', 'example_db')
MYSQL_TIMEOUT = mysql.getint('timeout', 5)
MYSQL_MAXSIZE = mysql.getint('maxsize', 5)

OPENVPN_UPDATE_INTERVAL = openvpn.getint('update_interval', 20)
OPENVPN_IFACE = openvpn.get('iface', 'tun0')
OPENVPN_HOST = openvpn.get('host', 'example.com')
OPENVPN_PORT = openvpn.getint('port', 1194)
OPENVPN_TIMEOUT = openvpn.getfloat('timeout', 2)

PROXY_CHECKER_URLS = ['https://browserleaks.com/ip', 'https://ip.brovpn.io/ip.php']
PROXY_CHECKER_UPDATE_INTERVAL = proxy_checker.getint('update_interval', 20)
PROXY_CHECKER_MAX_RETRY_COUNT = proxy_checker.getint('max_retry_count', 5)
PROXY_CHECKER_READ_TIMEOUT = proxy_checker.getint('read_timeout', 10)
PROXY_CHECKER_CONN_TIMEOUT = proxy_checker.getint('conn_timeout', 5)
PROXY_CHECKER_SCRIPT_OK = proxy_checker.get('script_ok', None)
PROXY_CHECKER_SCRIPT_BAD = proxy_checker.get('script_bad', None)
