# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

from utils.logger import Logger


class BaseParser(Logger):
    async def parse(self, proxy):
        pass
