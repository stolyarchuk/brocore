# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111


import warnings

from sshtunnel import (BaseSSHTunnelForwarderError,
                       HandlerSSHTunnelForwarderError, SSHTunnelForwarder)

from core.error import SshTunnelError
from utils.base_service import BaseService
from utils.config import (SSH_LOCAL_HOST, SSH_LOCAL_PORT, SSH_PRIVATE_KEY,
                          SSH_REMOTE_HOST, SSH_REMOTE_PORT, SSH_REMOTE_USER)


warnings.filterwarnings(action='ignore', module='.*paramiko.*')


class SshTunnel(BaseService):
    def __init__(self):
        super().__init__()
        self._server = SSHTunnelForwarder(
            (SSH_REMOTE_HOST, SSH_REMOTE_PORT),
            ssh_username=SSH_REMOTE_USER,
            ssh_private_key=SSH_PRIVATE_KEY,
            remote_bind_address=('127.0.0.1', 3306),
            local_bind_address=(SSH_LOCAL_HOST, SSH_LOCAL_PORT),
            logger=self._logger,
            set_keepalive=5
        )

    async def start(self, *args, **kwargs):
        try:
            self._server.start()
        except (HandlerSSHTunnelForwarderError, BaseSSHTunnelForwarderError) as err:
            await self.stop()
            raise SshTunnelError("%s" % err)

    async def stop(self):
        if self._server.is_active:
            self._server.close()

    async def reconnect(self):
        try:
            self._server.restart()
        except (HandlerSSHTunnelForwarderError, BaseSSHTunnelForwarderError) as err:
            self._logger.error("%s", err)
            raise SshTunnelError("%s" % err)
