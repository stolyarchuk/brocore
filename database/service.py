# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import asyncio

import aiomysql
from async_generator import async_generator, asynccontextmanager, yield_
from async_timeout import timeout
from pymysql.err import (DataError, InternalError, OperationalError, ProgrammingError)  # pylint: disable=ungrouped-imports
from database.ssh_tunnel import SshTunnel, SshTunnelError
from core.error import DatabaseConnectionError
from user_manager.user import User
from utils.base_service import BaseService
from utils.config import (MYSQL_DB, MYSQL_HOST, MYSQL_PASS, MYSQL_PORT,
                          MYSQL_TIMEOUT, MYSQL_USER, MYSQL_MAXSIZE)
from utils.helper import mode_required
from utils.timer import Timer


class AttrDict(dict):
    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            return None


class AttrDictCursor(aiomysql.DictCursor):
    dict_type = AttrDict


class SSAttrDictCursor(aiomysql.SSDictCursor):
    dict_type = AttrDict


class DataBase(BaseService):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pool = None
        self._restart_timer = Timer()
        self._ssh_tunnel = SshTunnel()

    async def start(self, *args, **kwargs):
        try:
            async with timeout(MYSQL_TIMEOUT):
                await self._ssh_tunnel.start(*args, **kwargs)
                await self._create_pool()
        except (SshTunnelError, OperationalError, asyncio.TimeoutError) as err:
            self._logger.error("%r", err)
            self._restart()

    async def _create_pool(self):
        self._pool = await aiomysql.create_pool(maxsize=MYSQL_MAXSIZE, host=MYSQL_HOST, port=MYSQL_PORT,
                                                user=MYSQL_USER, password=MYSQL_PASS,
                                                db=MYSQL_DB, connect_timeout=MYSQL_TIMEOUT,
                                                autocommit=True)

    async def stop(self):
        if self._restart_timer:
            self._restart_timer.stop()
        if self._pool:
            self._pool.close()
            await self._pool.wait_closed()
        if self._ssh_tunnel:
            await self._ssh_tunnel.stop()

    def _restart(self):
        self._restart_timer.start(self.reconnect, 5, 0)

    async def reconnect(self):
        try:
            asyncio.ensure_future(self.stop())
            asyncio.ensure_future(self.start())
        except asyncio.CancelledError:
            pass

    @asynccontextmanager
    @async_generator
    async def get_cursor(self, cursor):
        try:
            async with timeout(MYSQL_TIMEOUT):
                async with self._pool.acquire() as conn:
                    async with conn.cursor(cursor) as cur:
                        await yield_(cur)
        except (OperationalError, AttributeError) as err:
            self._logger.error("Query error! %s", err)
            self._restart()
        except asyncio.TimeoutError:
            self._logger.error("Query timeout!")
            self._restart()
        except asyncio.CancelledError:
            self._logger.warning("Query cancelled!")
        except (InternalError, ProgrammingError, DataError) as err:
            self._logger.error("%r", err)
        except (RuntimeError) as err:
            self._logger.error("%r", err)
            self._restart()
        except Exception as err:  # pylint: disable=broad-except # TODO: broad_except
            self._logger.warning("Broad exception! %r", err)

    async def make_user_from_db(self, vpnlogin, vpnip):
        user = User(vpnlogin=vpnlogin, vpnip=vpnip)

        query = "SELECT user,user_speed_in,user_speed_out FROM users_data WHERE vpnlogin=%s"
        query_data = (vpnlogin,)

        async with self.get_cursor(SSAttrDictCursor) as cur:
            await cur.execute(query, query_data)
            user_data = await cur.fetchone()

            if user_data:
                user.from_dict(**user_data)
            else:
                self._logger.warning("Cant fetch user %s from DB", vpnlogin)

        return user

    @mode_required(2)
    async def reset_user_status_by_node(self, hostname):
        query = "UPDATE users_data u JOIN nodes n ON u.user_node=n.id SET u.user_online=0,u.vpnip='' WHERE n.hostname=%s"
        query_data = ("{}.brovpn.io".format(hostname),)

        async with self.get_cursor(aiomysql.SSCursor) as cur:
            await cur.execute(query, query_data)

    @mode_required(2)
    async def set_user_status(self, user):
        query_data = (user.data.status, user.data.vpnip, user.data.vpnlogin)
        query = "UPDATE users_data SET user_online=%s, vpnip=%s WHERE vpnlogin=%s"

        async with self.get_cursor(aiomysql.SSCursor) as cur:
            await cur.execute(query, query_data)

    @mode_required(2)
    async def set_users_status(self, all_users_set):
        query_data = [(user.data.status, user.data.vpnip, user.data.vpnlogin) for user in all_users_set]
        query = "UPDATE users_data SET user_online=%s, vpnip=%s WHERE vpnlogin=%s"

        async with self.get_cursor(aiomysql.SSCursor) as cur:
            await cur.executemany(query, query_data)

    @mode_required(2)
    async def update_user_proxy(self, user: User):
        query = ("UPDATE users_data SET proxy_status=%s,last_ip=%s,"
                 "isp=%s,country=%s,"
                 "city=%s,connection_type=%s,"
                 "asn=%s,fingerprint=%s WHERE vpnlogin=%s")

        query_data = (user.proxy.info['proxy_status'], user.proxy.info['last_ip'],
                      user.proxy.info['isp'], user.proxy.info['country'],
                      user.proxy.info['city'], user.proxy.info['connection_type'],
                      user.proxy.info['asn'], user.proxy.info['fingerprint'], user.data.vpnlogin)

        async with self.get_cursor(aiomysql.SSCursor) as cur:
            await cur.execute(query, query_data)
