# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import os

from utils.config import OPENVPN_IFACE, MIN_RATE
from utils.logger import Logger
from utils.executor import Executor


if os.geteuid() != 0:
    TC = 'sudo tc'
else:
    TC = '/sbin/tc'


class TrafficControl(Logger):
    _flush_strings = [
        "%(tc)s qdisc del dev %(dev)s root",
        "%(tc)s qdisc del dev ifb0 root",
        "%(tc)s qdisc del dev %(dev)s ingress"
    ]
    _init_strings = [
        "%(tc)s qdisc add dev %(dev)s ingress",
        "%(tc)s filter add dev %(dev)s parent ffff: prio 1 protocol ip u32 match u32 0 0 flowid ffff:1 action connmark action mirred egress redirect dev ifb0",
    ]
    _post_init_strings = [
        "%(tc)s qdisc add dev %(dev)s root handle 1: htb",
        "%(tc)s class add dev %(dev)s parent 1: classid 1:1 htb rate 100Mbit ceil 100Mbit burst 10k",
        "%(tc)s class add dev %(dev)s parent 1:1 classid 1:10 htb rate %(min_rate)skbit ceil 100Mbit burst 10k",
    ]
    _add_strings = [
        "%(tc)s class add dev %(dev)s parent 1:10 classid 1:%(class_id)d htb rate %(min_rate)skbit ceil %(limit)skbit burst 10k",
        "%(tc)s qdisc add dev %(dev)s parent 1:%(class_id)d handle %(class_id)d: sfq perturb 10",
        "%(tc)s filter add dev %(dev)s parent 1: prio %(class_id)d protocol ip handle %(user_id)d fw flowid 1:%(class_id)d"
    ]
    _del_strings = [
        "%(tc)s filter del dev %(dev)s prio %(class_id)d",
        "%(tc)s class del dev %(dev)s classid 1:%(class_id)d"
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._executor = Executor()
        self._net_devs = ['ifb0', OPENVPN_IFACE]
        self._class_id = 101
        self._used_ids = set()

    async def start(self, *args, **kwargs):  # pylint: disable=unused-argument
        for command in self._flush_strings:
            await self._executor.run_async(command % {'tc': TC, 'dev': OPENVPN_IFACE}, errors=False)

        for command in self._init_strings:
            self._executor.run(command % {'tc': TC, 'dev': OPENVPN_IFACE})

        for dev in self._net_devs:
            for command in self._post_init_strings:
                self._executor.run(command % {'tc': TC, 'dev': dev, 'min_rate': MIN_RATE})

    async def stop(self):
        for command in self._flush_strings:
            await self._executor.run_async(command % {'tc': TC, 'dev': OPENVPN_IFACE}, errors=False)

    def _increment_class_id(self):
        if self._class_id == 999:
            self._class_id = 101
        else:
            self._class_id += 1

        if self._class_id in self._used_ids:
            self._increment_class_id()

    def add_rule(self, user):
        for dev in self._net_devs:
            for add_string in self._add_strings:
                self._executor.run(add_string % {'tc': TC, 'dev': dev, 'class_id': self._class_id,
                                                 'user_id': user.data.id, 'min_rate': MIN_RATE, 'limit': user.data.limit_in})

        user.data.class_id = self._class_id

        self._used_ids.add(self._class_id)
        self._increment_class_id()

    def del_rule(self, user):
        for dev in self._net_devs:
            for del_string in self._del_strings:
                if user.data.class_id:
                    self._executor.run(del_string % {'tc': TC, 'dev': dev, 'class_id': user.data.class_id})
                else:
                    self._logger.error("No such user :%s", user)
        if user.data.class_id in self._used_ids:
            self._used_ids.remove(user.data.class_id)
        else:
            self._logger.error("No such class :%d", user.data.class_id)
