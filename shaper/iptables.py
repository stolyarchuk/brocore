# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import os

from utils.config import OPENVPN_IFACE
from utils.logger import Logger
from utils.executor import Executor

if os.geteuid() != 0:
    IPT = 'sudo iptables -w 5'
else:
    IPT = '/sbin/iptables -w 5'


class Iptables(Logger):
    _flush_strings = [
        "%(ipt)s -t mangle -F PREROUTING",
        "%(ipt)s -t mangle -F POSTROUTING"
    ]
    _init_strings = [
        "%(ipt)s -t mangle -I PREROUTING -m set --match-set proxy_ports dst -m conntrack --ctstate NEW -j ACCEPT",
        "%(ipt)s -t mangle -I POSTROUTING -j CONNMARK --restore-mark"
    ]
    _add_string = "%(ipt)s -t mangle -A PREROUTING -i %(dev)s -s %(address)s -j CONNMARK --set-mark %(user_id)d"
    _del_string = "%(ipt)s -t mangle -D PREROUTING -i %(dev)s -s %(address)s -j CONNMARK --set-mark %(user_id)d"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._executor = Executor()

    async def start(self, *args, **kwargs):  # pylint: disable=unused-argument
        for command in self._flush_strings:
            await self._executor.run_async(command % {'ipt': IPT})

        for command in self._init_strings:
            await self._executor.run_async(command % {'ipt': IPT})

    async def stop(self):
        for command in self._flush_strings:
            await self._executor.run_async(command % {'ipt': IPT})

    async def add_rule(self, user):
        await self._executor.run_async(self._add_string % {'ipt': IPT, 'dev': OPENVPN_IFACE, 'address': user.data.vpnip, 'user_id': user.data.id})

    async def del_rule(self, user):
        await self._executor.run_async(self._del_string % {'ipt': IPT, 'dev': OPENVPN_IFACE, 'address': user.data.vpnip, 'user_id': user.data.id})
