# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903


from utils.logger import Logger
from shaper.iptables import Iptables
from shaper.traffic_control import TrafficControl


class Shaper(Logger):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._db = None
        self._iptables = Iptables()
        self._tc = TrafficControl()

    async def del_shape(self, user):
        await self._iptables.del_rule(user)
        self._tc.del_rule(user)
        self._logger.debug("Removed shape: %s", user)
        user.vpnip = ''

    async def add_shape(self, user):
        await self._iptables.add_rule(user)
        self._tc.add_rule(user)
        self._logger.debug("Added shape: %s", user)

    async def start(self, *args,  **kwargs):
        await self._iptables.start(**kwargs)
        await self._tc.start(**kwargs)
        self._db = kwargs['db']

    async def stop(self):
        await self._iptables.stop()
        await self._tc.stop()
