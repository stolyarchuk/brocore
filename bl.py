# -*- coding: utf-8 -*-

import asyncio
from collections import namedtuple

from aiohttp import ClientSession
from aiohttp.client_exceptions import (ClientHttpProxyError, ClientOSError,
                                       ClientProxyConnectionError,
                                       ServerDisconnectedError)

HttpResponse = namedtuple('HttpResponse', ['status', 'text', 'reason'])


async def fetch():
    try:
        async with ClientSession() as session:
            async with session.get('https://browserleaks.com/ip', ssl=False) as resp:
                text = await resp.text() if resp.status == 200 else ''
                return HttpResponse(resp.status, text, resp.reason)

    except (ClientOSError, ClientProxyConnectionError, ClientHttpProxyError, ServerDisconnectedError, TypeError, RuntimeError) as err:
        return HttpResponse(-1, '', "%r" % err)
    except (asyncio.TimeoutError, TimeoutError) as err:
        return HttpResponse(-1, '', 'Connection timeout!')
    except (asyncio.CancelledError) as err:
        return HttpResponse(-1, '', 'Request cancelled!')
    except Exception as err:
        print("Broad exception! %r" % err)
        return HttpResponse(-1, '', str(err))


loop = asyncio.get_event_loop()

response = loop.run_until_complete(fetch())


print(response)
