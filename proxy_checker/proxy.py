# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

from collections import OrderedDict
from utils.logger import Logger


class Proxy(Logger):
    def __init__(self, vpnip, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ip = vpnip
        self.port = 1507
        self.response = None
        self.info = OrderedDict([
            ('proxy_status', 'BAD'),
            ('last_ip', 'NA'),
            ('isp', 'NA'),
            ('country', 'NA'),
            ('region', 'NA'),
            ('city', 'NA'),
            ('connection_type', 'NA'),
            ('asn', 'NA'),
            ('fingerprint', 'NA')
        ])
        self.last_status = 'BAD'

    @property
    def status(self):
        return self.info['proxy_status'] == 'OK'

    @property
    def url(self):
        return "http://{}:{}".format(self.ip, self.port)

    @property
    def json(self):
        return self.info

    @property
    def is_valid(self):
        return "NA" not in self.info.values()

    def update(self, **kwargs):
        self.info.update(kwargs)

    def __str__(self):
        values = list(self.info.values())[:-1]
        values.append(self.response.status)
        values.append(self.response.reason)

        return "{}, {}, {}, {}, {}, {}, {}, {:.7} ({}, {})".format(*values)
