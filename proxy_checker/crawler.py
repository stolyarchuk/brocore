# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import asyncio
from collections import namedtuple

from aiohttp import ClientSession
from aiohttp.client_exceptions import (ClientHttpProxyError, ClientOSError,
                                       ClientProxyConnectionError,
                                       ServerDisconnectedError)

from utils.config import PROXY_CHECKER_URLS, PROXY_CHECKER_CONN_TIMEOUT, PROXY_CHECKER_READ_TIMEOUT
from utils.logger import Logger

from proxy_checker.parser import BLeaksParser, BrovpnParser

HttpResponse = namedtuple('HttpResponse', ['status', 'text', 'reason'])


class Crawler(Logger):
    def __init__(self):
        super().__init__()
        self.parsers = {
            PROXY_CHECKER_URLS[0]: BLeaksParser(),
            PROXY_CHECKER_URLS[1]: BrovpnParser()
        }

    async def fetch(self, proxy, checker):
        try:
            headers = {'User-Agent': checker.ua.random}
            async with ClientSession(read_timeout=PROXY_CHECKER_READ_TIMEOUT, conn_timeout=PROXY_CHECKER_CONN_TIMEOUT) as session:
                async with session.get(checker.url, headers=headers, proxy=proxy.url, ssl=False) as resp:
                    text = await resp.text() if resp.status == 200 else ''
                    return HttpResponse(resp.status, text, resp.reason)

        except (ClientOSError, ClientProxyConnectionError, ClientHttpProxyError, ServerDisconnectedError, TypeError, RuntimeError) as err:
            return HttpResponse(-1, '', "%r" % err)
        except (asyncio.TimeoutError, TimeoutError) as err:
            return HttpResponse(-1, '', 'Connection timeout!')
        except (asyncio.CancelledError) as err:
            return HttpResponse(-1, '', 'Request cancelled! (%s)' % checker.url)
        except Exception as err:  # pylint: disable=broad-except # TODO: broad_except
            self._logger.error("Broad exception! %r", err)
            return HttpResponse(-1, '', str(err))

    async def parse(self, user):
        parser = self.parsers.get(user.checker.url)

        if user.proxy.response.text:
            await parser.parse(user.proxy)
