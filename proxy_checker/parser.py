# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import json

from bs4 import BeautifulSoup
from lxml.etree import ParseError

from proxy_checker.proxy import Proxy
from utils.base_parser import BaseParser


class BLeaksParser(BaseParser):
    async def parse(self, proxy: Proxy):
        try:
            soup = BeautifulSoup(proxy.response.text, 'lxml')
            proxy.info['proxy_status'] = "OK"

            last_ip = soup.find('span', attrs={'id': 'client-ip'})
            if last_ip:
                proxy.info['last_ip'] = last_ip.string

            country = soup.find('span', attrs={'class': 'country-flag'})
            if country:
                proxy.info['country'] = country.string

            region_td = soup.find(lambda tag: tag.name == 'td' and tag.string == 'State/Region')
            if region_td and region_td.next_sibling.string:
                proxy.info['region'] = region_td.next_sibling.string

            city_td = soup.find(lambda tag: tag.name == 'td' and tag.string == 'City')
            if city_td and city_td.next_sibling.string:
                proxy.info['city'] = city_td.next_sibling.string

            asn_td = soup.find(lambda tag: tag.name == 'td' and tag.string == 'ASN')
            if asn_td and asn_td.next_sibling.string:
                proxy.info['asn'] = asn_td.next_sibling.string

            isp_td = soup.find(lambda tag: tag.name == 'td' and tag.string == 'ISP')
            if isp_td and isp_td.next_sibling.string:
                proxy.info['isp'] = isp_td.next_sibling.string

            connection_td = soup.find(lambda tag: tag.name == 'td' and tag.string == 'Connection Type')
            if connection_td and connection_td.next_sibling.string:
                proxy.info['connection_type'] = connection_td.next_sibling.string

            fingerprint_td = soup.find(lambda tag: tag.name == 'td' and tag.text == 'Passive, SYN')
            if fingerprint_td and fingerprint_td.next_sibling.string:
                proxy.info['fingerprint'] = fingerprint_td.next_sibling.string

        except (KeyError, ValueError, ParseError) as err:
            self._logger.warning("Proxy: %s; %s", proxy, err)


class BrovpnParser(BaseParser):
    async def parse(self, proxy: Proxy):
        try:
            data_json = json.loads(proxy.response.text)
            proxy.info['proxy_status'] = "OK"
            proxy.info['last_ip'] = data_json.get('ip', "NA")
            proxy.info['isp'] = data_json.get('isp', "NA")
            proxy.info['country'] = "%s (%s)" % (data_json.get('country', "NA"), data_json.get('countryCode', "NA"))
            proxy.info['region'] = "%s (%s)" % (data_json.get('regionName', "NA"), data_json.get('region', "NA"))
            proxy.info['city'] = data_json.get('city', "NA")
            proxy.info['connection_type'] = "Cellular" if data_json.get('mobile', False) else "Cable/DSL"
            proxy.info['asn'] = data_json.get('as', "NA")
            proxy.info['fingerprint'] = "%s | Link: %s | MTU: %s | Distance: %s Hops" % (
                data_json.get('os', "NA"),
                data_json.get('link', "NA"),
                data_json.get('mtu', "NA"),
                data_json.get('dist', "NA"))

        except (KeyError, ValueError, json.JSONDecodeError) as err:
            self._logger.warning("Proxy: %s; %s", proxy, err)
