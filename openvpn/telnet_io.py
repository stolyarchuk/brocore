# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import asyncio
from collections import namedtuple

from async_generator import async_generator, asynccontextmanager, yield_

from utils.config import OPENVPN_HOST, OPENVPN_PORT, OPENVPN_TIMEOUT
from utils.logger import Logger

ENDL = b"\r\n"


class TelnetIO(Logger):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._banner = b">INFO:OpenVPN Management Interface Version 1 -- type 'help' for more info\r\n"
        self._reader = None
        self._writer = None

    @asynccontextmanager
    @async_generator
    async def connect(self):
        try:
            await self._connect(host=OPENVPN_HOST, port=OPENVPN_PORT)
            await yield_()
        finally:
            await self._close()

    async def communicate(self, cmd, end=""):
        try:
            cmd_bytes = cmd.encode() + ENDL
            end_bytes = end.encode() + ENDL

            async with self.connect():
                await self._read_until()
                await self._write(cmd_bytes)

                return await self._read_until(end_bytes)

        except (ConnectionRefusedError, RuntimeError, asyncio.IncompleteReadError) as err:
            self._logger.error("Error talking to openvpn: %r", err)
        except asyncio.CancelledError:
            self._logger.warning("Cancelled talking to openvpn")
        except asyncio.TimeoutError as err:
            self._logger.error("Timeout talking to openvpn")
        except Exception as err:  # pylint: disable=broad-except # TODO: broad_except
            self._logger.error("Broad exception: %r", err)

    async def _connect(self, host, port):
        self._reader, self._writer = await asyncio.wait_for(asyncio.open_connection(host, port), timeout=OPENVPN_TIMEOUT)

    async def _close(self):
        try:
            if self._writer.can_write_eof():
                self._writer.write_eof()
            self._writer.close()
            await asyncio.sleep(0)
        except AttributeError:
            pass

    async def _read_until(self, separator=b'\r\n'):
        return await asyncio.wait_for(self._reader.readuntil(separator), timeout=OPENVPN_TIMEOUT)

    async def _write(self, data):
        self._writer.write(data)
        await asyncio.wait_for(self._writer.drain(), timeout=OPENVPN_TIMEOUT)
