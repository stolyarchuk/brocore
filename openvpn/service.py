# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

from collections import namedtuple

from utils.helper import mode_required
from utils.base_service import BaseService
from openvpn.telnet_io import TelnetIO, ENDL

OpenvpnResponse = namedtuple('OpenvpnResponse', ['data', 'error'])


class OpenVPN(BaseService):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._console = TelnetIO()

    async def get_tuples(self):
        reply = await self._console.communicate("status 3", "END")
        tuples = set()

        if reply is None:
            return OpenvpnResponse(data=tuples, error=True)

        for line in reply.split(ENDL):
            words = line.split(b"\t")
            if words[0] == b"CLIENT_LIST":
                words_decoded = [word.decode() for word in words]
                tuples.add((words_decoded[1], words_decoded[3]))

        return OpenvpnResponse(data=tuples, error=False)

    @mode_required(3)
    async def kill_user(self, vpnlogin):
        await self._console.communicate("kill {}".format(vpnlogin))
        self._logger.debug("killed user: %s", vpnlogin)
