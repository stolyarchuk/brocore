# -*- coding: utf-8 -*-

import asyncio
import socket
import signal

from collections import OrderedDict

from api.service import Api
from utils.base_service import BaseService
from core.error import CoreError, SshTunnelError, DatabaseConnectionError
from core.stats import CoreStats
from database.service import DataBase
from openvpn.service import OpenVPN
from rpc.service import Rpc
from task_pool.service import TaskPool
from user_manager.service import UserManager


class Core(BaseService):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._hostname = socket.gethostname()
        self._services = OrderedDict()
        self._services['core_stats'] = CoreStats()
        self._services['task_pool'] = TaskPool()
        self._services['db'] = DataBase()
        self._services['openvpn'] = OpenVPN()
        self._services['user_manager'] = UserManager()
        self._services['rpc'] = Rpc()
        self._services['api'] = Api()

    async def start(self, *args, **kwargs):
        loop, = args

        loop.add_signal_handler(signal.SIGTERM, lambda: asyncio.ensure_future(self.exit(loop)))
        loop.add_signal_handler(signal.SIGINT, lambda: asyncio.ensure_future(self.exit(loop)))

        try:
            for _, service in self._services.items():
                await service.start(**self._services, hostname=self._hostname)

            self._logger.info("Core started on %s", self._hostname)
        except (SshTunnelError, DatabaseConnectionError) as err:

            self._logger.error("%s", err)
        except CoreError as err:
            self._logger.error("%s", err)
            raise

    async def stop(self):
        for _, service in reversed(self._services.items()):
            await service.stop()

        self._logger.info("Core stopped on %s", self._hostname)

    async def exit(self, loop):
        loop.remove_signal_handler(signal.SIGTERM)
        loop.remove_signal_handler(signal.SIGINT)

        try:
            await asyncio.wait_for(self.stop(), 60)
        except (asyncio.TimeoutError, asyncio.CancelledError):
            pass

        loop.stop()

    @property
    def items(self):
        return self._services

    def __getattr__(self, name):
        return self._services.get(name, None)
