# -*- coding: utf-8 -*-


import os
import psutil

from utils.base_service import BaseService
from utils.executor import Executor


class CoreStats(BaseService):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._proc = psutil.Process(os.getpid())
        self.version, _ = Executor().run('git rev-parse --short HEAD')

    @property
    def cpu_usage(self):
        return self._proc.cpu_percent()
