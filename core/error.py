# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903


class CoreError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)


class DatabaseConnectionError(CoreError):
    def __init__(self, *args, **kwargs):
        CoreError.__init__(self, *args, **kwargs)


class SshTunnelError(CoreError):
    def __init__(self, *args, **kwargs):
        CoreError.__init__(self, *args, **kwargs)


class ProxyCheckerLimitError(CoreError):
    def __init__(self, *args, **kwargs):
        CoreError.__init__(self, *args, **kwargs)
