# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import asyncio
from utils.logger import Logger


class Task(Logger):
    def __init__(self, coro, prio):
        super().__init__()
        self.prio = prio
        self.future = asyncio.ensure_future(coro) if coro else None

    async def run(self):
        try:
            return await asyncio.wait_for(self.future, 20)
        except (asyncio.CancelledError, asyncio.TimeoutError, RuntimeError, TypeError) as err:
            self._logger.warning("%r", err)
        except Exception as err:  # pylint: disable=broad-except # TODO: broad_except
            self._logger.warning("Broad exception! %r", err)

    def done(self):
        return self.future.done() if self.future else True

    def cancelled(self):
        return self.future.cancelled() if self.future else True

    def __lt__(self, other):
        return self.prio < other.prio

    def __eq__(self, other):
        return self.prio == other.prio

    def cancel(self):
        if self.future:
            self.future.cancel()
