# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import asyncio
from asyncio.queues import PriorityQueue

from task_pool.task import Task
from utils.config import QUEUE_MAX_SIZE, QUEUE_WORKERS
from utils.base_service import BaseService


class TaskPool(BaseService):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._queue = PriorityQueue(QUEUE_MAX_SIZE)
        self._workers = []

    async def _worker(self):
        while True:
            task = await self._queue.get()

            if task.future is None:
                break

            await task.run()

            self._queue.task_done()

    def put_nowait(self, coro, prio=5):
        try:
            self._queue.put_nowait(Task(coro, prio))
        except asyncio.queues.QueueFull:
            self._logger.warning("Queue is full: (%d/%d)", self.qsize, QUEUE_MAX_SIZE)
            self._clear()

    def _clear(self):
        for _ in range(self.qsize):
            self._queue.get_nowait().cancel()

    async def start(self, *args, **kwargs):
        for _ in range(QUEUE_WORKERS):
            worker = asyncio.ensure_future(self._worker())
            self._workers.append(worker)

    async def stop(self):
        try:
            self._clear()
            for _ in self._workers:
                self.put_nowait(None, 99)
            await asyncio.wait_for(asyncio.gather(*self._workers), 20)
        except (asyncio.QueueFull, asyncio.TimeoutError, asyncio.CancelledError):
            pass

    @property
    def qsize(self):
        return self._queue.qsize()
