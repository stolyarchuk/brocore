# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

from api.views import IndexView, UserView

routes = [
    ('GET', '/', IndexView, 'index'),
    ('GET', '/user', UserView, 'user'),
]
