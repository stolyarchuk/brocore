# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import json
from datetime import date, datetime
from collections import OrderedDict

from aiohttp import web

from utils.logger import Logger


def complex_encoder(o):
    if isinstance(o, datetime):
        return o.strftime("%c")
    if isinstance(o, date):
        return str(o)

    return str(o)


class ApiView(web.View, Logger):  # pylint: disable=too-many-ancestors

    def parse_url(self):
        resource_id = self.request.match_info.get('id')
        prop = self.request.match_info.get('prop')

        return resource_id, prop

    def response(self, text=None):
        self._logger.debug("request from %s: %s, response: %s", self.peername, self.request.path_qs, text)
        return web.Response(text=text)

    def json_response(self, data, **kwargs):
        return web.json_response(data, dumps=self.complex_encoder, **kwargs)

    def bad_response(self, data=None, status=400):
        self._logger.warning("Bad request from %s: %s, data: %s", self.peername, self.request.path_qs, data)
        return web.Response(status=status)

    def not_found(self, status=404):
        self._logger.warning("Bad request from %s: %s", self.peername, self.request.path_qs)
        return web.Response(status=status)

    @property
    def peername(self):
        return self.request.transport.get_extra_info('peername')

    @staticmethod
    def complex_encoder(data):
        return json.dumps(data, default=complex_encoder, sort_keys=False)


class IndexView(ApiView):  # pylint: disable=too-many-ancestors

    async def get(self):
        return await self.get_index(self.request)

    async def get_index(self, request):
        json_dict = OrderedDict([
            ('hostname', request.app.hostname),
            ('version', request.app.core_stats.version),
            ('started', request.app.started),
            ('cpu_usage', request.app.core_stats.cpu_usage),
            ('total_users', len(request.app.user_manager.users)),
            ('queue_size', request.app.task_pool.qsize)
        ])

        return self.json_response(json_dict)


class UserView(ApiView):  # pylint: disable=too-many-ancestors

    async def get(self):
        return await self.get_users(self.request)

    async def get_users(self, request):
        users = request.app.user_manager.users

        json_dict = OrderedDict([
            ('total', len(users)),
            ('data', [user.json for user in users])
        ])

        return self.json_response(json_dict)
