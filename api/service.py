# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=C0111, R0903

import asyncio
from datetime import datetime

from aiohttp import web

from api.routes import routes
from utils.base_service import BaseService
from utils.config import API_LISTEN, API_PORT


class Api(BaseService):

    def __init__(self):
        super().__init__()
        self.app = None
        self._handler = None
        self._server = None

    async def start(self, *args, **kwargs):
        if API_PORT:
            loop = asyncio.get_event_loop()

            self.app = web.Application()
            self.app.user_manager = kwargs['user_manager']
            self.app.task_pool = kwargs['task_pool']
            self.app.core_stats = kwargs['core_stats']
            self.app.hostname = kwargs['hostname']
            self.app.started = datetime.now()

            for route in routes:
                route = self.app.router.add_route(route[0], route[1], route[2], name=route[3])

            self._handler = self.app.make_handler(logger=self._logger)
            self._server = await loop.create_server(self._handler, API_LISTEN, API_PORT)
            self._logger.info("API started on %s, listening %s", self.app.hostname, self._server.sockets[0].getsockname())

    async def stop(self):
        try:
            self._server.close()
            await self._server.wait_closed()
            await self._handler.shutdown(5)
            await self.app.shutdown()
            await self.app.cleanup()
        except AttributeError:
            pass
